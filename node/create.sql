CREATE DATABASE IF NOT EXISTS iot;
USE iot;
CREATE TABLE IF NOT EXISTS plante (
     id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
     nom VARCHAR(255) NOT NULL,
     duree INT(4) DEFAULT 2,
     pompe INT(2) DEFAULT 0,
     dernier_arrosage DATETIME DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS conseil (
     id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
     plante VARCHAR(255),
     description VARCHAR(255) NOT NULL
);
INSERT INTO conseil (plante, description) VALUES ('Cactus', 'Did you know the cactus has over 1700 species ?');
