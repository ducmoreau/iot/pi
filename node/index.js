require('dotenv').config()

const express = require('express')
const app = express()

const say = require('say')

const colors = require('colors');

//Seconds
const loopInterval = 1500

const mysql = require('mysql')
const sql = mysql.createConnection({
	host: process.env.MYSQL_HOST,
	database: process.env.MYSQL_DATABASE,
	user: process.env.MYSQL_USER,
	password: process.env.MYSQL_PASSWORD
})
sql.connect()

var SerialPort = require('serialport')
var serialPort = new SerialPort(process.env.SERIAL_PORT, {
	baudRate: parseInt(process.env.SERIAL_BAUDRATE)
})

const http = require('http')
app.use(express.static('./public')) // where the web page code goes
const http_server = http.createServer(app).listen(3000)

const receivedSerialBufferLenght = 3
serialBufferIndex = 0

/*
 * Speech to text
 */
const recorder = require('node-record-lpcm16');
const speech = require('@google-cloud/speech');
const client = new speech.SpeechClient();

const encoding = 'LINEAR16'
const sampleRateHertz = 16000
const languageCode = 'fr-FR'
const request = {
	config: {
		encoding: encoding,
		sampleRateHertz: sampleRateHertz,
		languageCode: languageCode,
	},
	interimResults: false, // If you want interim results, set this to true
}

waterRemaining = true
temperature = 22
humidity = 38

// Main function
let reponseInterval = false
let plante = ''

function processTranscription(text) {

	// Réponses prioritaires
	if (reponseInterval) {
		let textArray = text.split(" ")
		let nbJours = 0
		for (var i = 0; i < textArray.length; i++) {
			if (textArray[i] == 'jour' || textArray[i] == 'jours') {
				reponseInterval = false
				break
			}
			nbJours = textArray[i]
		}
		console.log('Nombre de jours : ' + nbJours)
		if (nbJours !== parseInt(nbJours, 10)) nbJours = 2
		insertPlante(plante, nbJours, 1, new Date().toMysqlFormat())
		sayWithLog('Ok')
	}

	console.log(colors.cyan('> ' + text))

	// Use case : test
	if (text.includes('test')) {
		console.log('Test')
	}

	// Use case : insertion d'une nouvelle plante
	else if (text.includes('ajoute') && text.includes('plante')) {
		let textArray = text.split(" ")
		plante = ''
		for (var i = textArray.length - 1; i >= 0; i--) {
			if (textArray[i] == 'plante') break
			plante = textArray[i] + ' ' + plante
		}
		console.log('Ajout de la plante : ' + plante)
		sayWithLog('How often should I water the plant ?')
		reponseInterval = true
	}

	// Use case : arrosage manuel
	else if (text.includes('bon') && (text.includes('arrosé') || text.includes('arroser'))) {
		//sayWithLog('Well done !')
		updateArrosage(plante, 0, new Date().toMysqlFormat())
		sql.query('SELECT description FROM conseil WHERE plante = "cactus";', function(err, rows, fields) {
			if (err) throw err
			sayWithLog('Well done ! Now fun fact : ' + rows[0].description)
		})
	}

	// Use case : besoin d'arroser
	else if (text.includes('besoin') && (text.includes('d\'arrosé') || text.includes('d\'arroser'))) {
		checkPlants()
	}
	
	else if (text.includes('température') && text.includes('humidité')) {
		sayTemperatureHumidity()
	}

	else if (text.includes('température')) {
		sayTemperature()
	}

	else if (text.includes('humidité')) {
		sayHumidity()
	}

	else if (text.includes('arrose') && text.includes('plante')){
		waterPlant(2)
	}

}

// Create a recognize stream
const recognizeStream = client
	.streamingRecognize(request)
	.on('error', console.error)
	.on('data', data => {
		if (data.results[0] && data.results[0].alternatives[0]) {
			processTranscription(data.results[0].alternatives[0].transcript)
		} else {
			process.stdout.write(`\n\nReached transcription time limit, press Ctrl+C\n`)
		}
	});


main()

function main(){
	console.log('Ready')

	loop()

	waterPlant(0.1) // create connection

	setInterval(function(){
		loop()
	}, loopInterval*1000);

	recorder
		.record({
			sampleRateHertz: sampleRateHertz,
			threshold: 0,
			// Other options, see https://www.npmjs.com/package/node-record-lpcm16#options
			verbose: false,
			recordProgram: 'rec', // Try also "arecord" or "sox"
			silence: '2.0',
		})
		.stream()
		.on('error', console.error)
		.pipe(recognizeStream)
	console.log('Recording...')
}

function loop(){
	checkPlants()
}

function insertPlante(plante, duree, pompe) {
	console.log('Insertion d\'une nouvelle plante : ' + plante + ', qui sera arrosée tous les ' + duree + ' jours à la pompe n°' + pompe)
	sql.query('INSERT INTO plante (nom, duree, pompe) VALUES (?, ?, ?)', [plante, duree, pompe], function(err, rows, fields) {
		if (err) throw err
		console.log('Nouvelle plante insérée')
	})
}

function checkPlants(){
	sql.query("SELECT * FROM plante", function (err, result, fields) {
		if (err) throw err
		parsePlants(result)
	})
}

// heure, heure
function needWater(timeLastWatering, duration){
	if(isHotOrDry()){
		return (timeLastWatering > duration/2)
	}else{
		return (timeLastWatering > duration)
	}
}

function isHotOrDry(){
	return ( temperature > 25 || humidity < 35 )
}

// ======================================================================

function sayWaterRemaining(){
	if(waterRemaining){
		text = 'There is enough water'
	}else{
		text = 'There is no more water'
	}
	sayWithLog( text )
}

function sayTemperature(){
	text = 'Temperature is ' + temperature + ' degree centigrade'
	sayWithLog( text )
}

function updateArrosage(plante, pompe, dernierArrosage) {
	console.log('Modification de la plante : ' + plante + ' à la pompe ' + pompe)
	sql.query('UPDATE plante SET dernier_arrosage = ? WHERE nom = ? AND pompe = ?', [dernierArrosage, plante, pompe], function(err, rows, fields) {
		if (err) throw err
		console.log('Arrosage de la plante mise à jour')
	})
}

function sayHumidity(){
	text = 'Humidity is ' + humidity + ' per cent'
	sayWithLog( text )
}

function sayTemperatureHumidity(){
	text = 'Temperature is ' + temperature + ' degree centigrade and humidity is ' + humidity + ' per cent'
	sayWithLog( text )
}

// ======================================================================    SAY

function sayWithLog( text ){
	console.log( colors.magenta( '> ' + text ) )
	say.speak( text )
}

// ======================================================================    Serial

function writeSerial(data){
	charCode = Math.round(data.duration*10)
	char = String.fromCharCode(charCode)
	console.log('Sending \'' + char + '\' (ASCII ' + charCode + ')')
	serialPort.write(char)
}

function parseSerial(data){
	i = 0
	while(i < receivedSerialBufferLenght && data[i] != undefined){

		switch (serialBufferIndex) {
			case 0:
				waterRemaining = (data[i] == "2")
				// console.log('Water remaining' + ' : ' + waterRemaining)
				break;
			case 1:
				temperature = parseInt(data[i])/2
				// console.log('Temperature' + ' : ' + temperature)
				break;
			case 2:
				humidity = parseInt(data[i])
				// console.log('Humidity' + ' : ' + humidity)
				break;
		}

		if(serialBufferIndex == receivedSerialBufferLenght - 1 ){
			serialBufferIndex = 0
		}else{
			serialBufferIndex++
		}
		i++
	}
}

serialPort.on('open', function(){
	console.log('Serial port opened')

	serialPort.on('data', function(data){
		console.log('Received serial data')
		parseSerial(data)
	})
})

// ======================================================================    DB

function parsePlants(plants){
	for (i = 0; i < plants.length; i++) {
		plant = plants[i]
		lastWatering = new Date(plant.dernier_arrosage)
		duration = plant.duree

		// difference en heure, arrondi à 2 chiffres
		difference = (lastWatering - Date.now())/-3600000
		difference = (Math.round(difference * 100) / 100)

		needWater = (
			isHotOrDry() && (difference > duration/2)
		) || (
			!isHotOrDry() && (difference > duration)
		)

		console.log(
			'plante ' + plant.id + ' (' + plant.nom + ') ' + duration + 'h : ' + difference + 'h '
			+ colors.blue(( needWater ? 'NEED WATER ' : 'NEED NOT ')) + ( isHotOrDry() ? '(hot/dry)' : '(normal)' )
		)

		if(needWater){
			if(plant.pompe != 0){
				text = 'the plant ' + plant.nom + ' needs water'
			}else{
				text = 'the plant ' + plant.nom + ' needs water'
			}
			sayWithLog(text)
		}
	}
}

function deletePlante(plante, pompe) {
	console.log('Suppression de la plante : ' + plante)
	sql.query('DELETE FROM plante WHERE UPPERCASE(nom) = UPPERCASE(?) AND pompe = ?', [plante, pompe], function(err, rows, fields) {
		if (err) throw err
		console.log('Plante supprimée')
	})
}

function waterPlant(duration){
	writeSerial({duration: duration})
}

// ======================================================================

function twoDigits(d) {
	if(0 <= d && d < 10)
		return "0" + d.toString()

	if(-10 < d && d < 0)
		return "-0" + (-1*d).toString()

	return d.toString();
}

Date.prototype.toMysqlFormat = function() {
	return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds())
}
