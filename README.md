# README

### Prérequis

Sous windows : 

* installer sox 14.4.1 (la .2 marche pas)

* ajouter une variable d'environnement GOOGLE_APPLICATION_CREDENTIALS=C:/path/to/bougnadget.json (fichier pour lien avec google cloud api)

Sous linux :

* installer sox

* ajouter dans le ~/.bashrc la ligne suivante : export GOOGLE_APPLICATION_CREDENTIALS="/home/pi/iot/pi/node/bougnadget.json"

Puis

```bash

npm install

npm run build

npm start

```